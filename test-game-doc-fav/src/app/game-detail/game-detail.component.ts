import { Component, OnInit, Input, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Game } from '../models/game';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-game-detail',
  standalone:true,
  imports:[CommonModule],
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent {
  @Input() public game!: Game;

	constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
    console.log(this.game);
  }
}
