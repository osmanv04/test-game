import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Game } from '../models/game';


@Injectable({
  providedIn: 'root'
})
export class ListGameService {
  constructor(private http: HttpClient) { }


  getAllGames(): Observable<Game[]>{
    // const headers = new HttpHeaders({
    //   Authorization: `Bearer ${localStorage.getItem('auth_token')}`
    // })
    return this.http.get<Game[]>('https://cors-anywhere.herokuapp.com/https://www.freetogame.com/api/games');
  }
}
