import { Genre } from './../models/genre';
import { Game } from './../models/game';
import { GameDetailComponent } from './../game-detail/game-detail.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgFor, CommonModule } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { ListGameService } from '../services/list-game.service';
import { map, startWith } from 'rxjs/operators';
import { Observable, combineLatest, of } from 'rxjs';


@Component({
    selector: 'app-table-game',
    templateUrl: './table-game.component.html',
    standalone: true,
    providers: [ListGameService],
    styleUrls: ['./table-game.component.scss'],
    imports: [NgFor, FormsModule, NgbTypeaheadModule, NgbPaginationModule, CommonModule, GameDetailComponent,ReactiveFormsModule]
})
export class TableGameComponent implements OnInit {
  page:number = 1;
	pageSize:number = 20;
	games:Game[] = [];
  filteredGames:Game[]=[];
  genres:string[] = [];
  platforms: string[] = [];
  gameDetail!:Game;
  searchForm: any;
	collectionSize = 0;
  filteredGames$: Observable<Game[]> = new Observable<Game[]>;
  @ViewChild(GameDetailComponent)
  gameDetailComponent!: GameDetailComponent;
	constructor(
    private _gameService: ListGameService,
    private _modalService: NgbModal,
    public fb: FormBuilder
  ) {
    this.searchForm = this.createForm();
    this.getAll();

	}

  ngOnInit(): void {
    //this.getValuesForFilter()
  }


  createForm(){

    return (new FormGroup({
      nameFormControl: new FormControl(''),
      genreFormControl: new FormControl(''),
      platformFormControl: new FormControl(''),
    }));
  }

  get name(): string {
    return this.searchForm.get('nameFormControl').value;
  }

  get genre(): string {
    return this.searchForm.get('genreFormControl').value;
  }

  get platform():string {
    return this.searchForm.get('platformFormControl').value;
  }
  getAll(){
    let games$ =  this._gameService.getAllGames();
    games$.subscribe(games=>{
      this.games = games;
      this.filteredGames = this.games;
      this.genres = [... new Set(games.map(x=>x.genre))];
      this.platforms = [...new Set(games.map(x=>x.platform))];
    })
  }

  onSearch(event:any){
    this.filteredGames = this.games.filter(x=>{
      return x.title.toLowerCase().includes(this.name.toLowerCase()) &&
             x.genre.toLowerCase().includes(this.genre.toLowerCase()) &&
             x.platform.toLowerCase().includes(this.platform.toLowerCase())
    })
  }
  openDetail(game:Game){
    const modalRef = this._modalService.open(GameDetailComponent);
    modalRef.componentInstance.game = game;
    modalRef.result.then((result) => {
      if (result) {
        console.log(result);
      }
    });
  }
}
